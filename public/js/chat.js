const socket = io();

//disable the button
const $form = document.querySelector("#form-msg");
const $formInput = $form.querySelector("input");
const $formButton = $form.querySelector("button");
// disable for location
const $sendLocation = document.querySelector("#send-location");
const $messages = document.querySelector("#messages");
//msg-template
const messageTemplate = document.querySelector("#message-Template").innerHTML;
// sidebar
const sidebaTemp = document.querySelector("#sidebar-template").innerHTML;

//Querystring
const { username, room } = Qs.parse(location.search, {
  ignoreQueryPrefix: true,
});
// Autoscroll for Message
const autoscroll = () => {
  const $newMessage = $messages.lastElementChild;
  // height of message
  const newMessageStyle = getComputedStyle($newMessage);
  const newMessageMargin = parseInt(newMessageStyle.marginBottom);
  const newMessageHeight = $newMessage.offsetHeight + newMessageMargin;

  const visibleHeight = $messages.offsetHeight;
  const containerHeight = $messages.scrollHeight;
  // how far scrolled
  const scrollOffset = $messages.scrollTop + visibleHeight;
  if (containerHeight - newMessageHeight <= scrollOffset) {
    $messages.scrollTop = $messages.scrollHeight;
  }
};
//location url
const locationMsgTemplate = document.querySelector(
  "#location-message-Template"
).innerHTML;
socket.on("message", (message) => {
  console.log(message);
  // add messages to template
  const html = Mustache.render(messageTemplate, {
    username: message.username,
    message: message.text,
    createdAt: moment(message.createdAt).format("h:mm a"),
  });
  $messages.insertAdjacentHTML("beforeend", html);
});
// added location url
socket.on("locationMessage", (message) => {
  console.log(message);
  const html = Mustache.render(locationMsgTemplate, {
    username: message.username,
    url: message.url,
    createdAt: moment(message.createdAt).format("h:mm a"),
  });
  $messages.insertAdjacentHTML("beforeend", html);
});
// client side roomData
socket.on("roomData", ({ room, users }) => {
  const html = Mustache.render(sidebaTemp, {
    room,
    users,
  });
  document.querySelector("#sidebar").innerHTML = html;
});
document.querySelector("#form-msg").addEventListener("submit", (e) => {
  e.preventDefault();
  // enable
  $formButton.setAttribute("disabled", "disabled");

  const message = e.target.elements.message.value;

  socket.emit("sendMessage", message, (error) => {
    // disable
    $formButton.removeAttribute("disabled");
    $formInput.value = "";
    $formInput.focus();
    if (error) {
      return console.log(error);
    }
    console.log("message is delivered ");
  });
});

// position of user
$sendLocation.addEventListener("click", () => {
  if (!navigator.geolocation) {
    return alert("your browser cannot support ");
  }
  $sendLocation.setAttribute("disabled", "disabled");
  navigator.geolocation.getCurrentPosition((position) => {
    socket.emit(
      "sendLocation",
      {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      },
      () => {
        $sendLocation.removeAttribute("disabled");
        console.log("location is shared");
      }
    );
  });
});
// client side emit
socket.emit("join", { username, room }, (error) => {
  if (error) {
    location.href = "/";
  }
});
