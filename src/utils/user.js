const users = [];

// adduser
export const addUser = ({ id, username, room }) => {
  // for case sensitive
  username = username.trim().toLowerCase();
  room = room.trim().toLowerCase();
  // validate user
  if (!username || !room) {
    return {
      error: "Username and room must be required!",
    };
  }
  // check existing user
  const existingUser = users.find((user) => {
    return user.room === room && user.username === username;
  });
  //console.log(existingUser);
  if (existingUser) {
    return {
      error: "Username is already taken!",
    };
  }
  // store user
  const user = { id, username, room };
  users.push(user);
  return {
    user,
  };
};
// removeUser
export const removeUser = (id) => {
  const index = users.findIndex((user) => user.id === id);
  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
};
// get user by Id
export const getUser = (id) => {
  return users.find((user) => user.id === id);
};
// get userIndsideRoom
export const getUserInRoom = (room) => {
  room = room.trim().toLowerCase();
  return users.filter((user) => user.room === room);
};
