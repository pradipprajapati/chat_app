import express from "express";
import { createServer } from "http";
import { Server } from "socket.io";
import { join, dirname } from "path";
import { fileURLToPath } from "url";
import Filter from "bad-words";
import { generateMessage, generateLocationMessage } from "./utils/message.js";
import { addUser, removeUser, getUser, getUserInRoom } from "./utils/user.js";

const __filename = fileURLToPath(import.meta.url);

const app = express();
const server = createServer(app);
const io = new Server(server);

const __dirname = dirname(__filename);

// add public folder
app.use(express.static(join(__dirname, "../public")));

io.on("connection", (socket) => {
  console.log("user is connected");

  // server on
  socket.on("join", (options, callback) => {
    const { user, error } = addUser({ id: socket.id, ...options });
    if (error) {
      return callback(error);
    }

    socket.join(user.room);
    socket.emit("message", generateMessage("Admin", "welcome"));
    // broadcast msg when new user join
    socket.broadcast
      .to(user.room)
      .emit("message", generateMessage("Admin", `${user.username} is joined`));
    io.to(user.room).emit("roomData", {
      room: user.room,
      users: getUserInRoom(user.room),
    });
    callback();
  });
  socket.on("sendMessage", (message, callback) => {
    const user = getUser(socket.id);
    const filter = new Filter();
    if (filter.isProfane(message)) {
      return callback("profinity is not allowed");
    }
    io.to(user.room).emit("message", generateMessage(user.username, message));
    callback();
  });
  // sending location
  socket.on("sendLocation", (coords, callback) => {
    const user = getUser(socket.id);
    io.to(user.room).emit(
      "locationMessage",
      generateLocationMessage(
        user.username,
        `https://google.com/maps?q=${coords.latitude},${coords.longitude}`
      )
    );
    callback();
  });
  socket.on("disconnect", () => {
    const user = removeUser(socket.id);
    if (user) {
      io.to(user.room).emit(
        "message",
        generateMessage("Admin", `${user.username} has left`)
      );
      io.to(user.room).emit("roomData", {
        room: user.room,
        users: getUserInRoom(user.room),
      });
    }
  });
});

// server setup
server.listen(process.env.PORT, () => {
  console.log(`Server running at ${process.env.PORT}`);
});
